#include <stdlib.h>
#include <stdio.h>
#include "ei_types.h"
#include "ei_main.h"
#include "ei_draw.h"
#include "ei_event.h"
#include "hw_interface.h"
#include <stdbool.h>
#include <ei_types.h>
#include <ei_event.h>
#include <allegro5/haptic.h>

ei_color_t white = {0xff, 0xff, 0xff, 0xaa};

void moveKeyboard(ei_surface_t *window, ei_surface_t *images, ei_point_t **positions, int speed, bool *pressed_keys) {
	hw_surface_lock(*window);
	ei_fill(*window, &white, EI_FALSE);

	ei_point_t *pos = positions[0];
	int window_width = hw_surface_get_rect(*window).size.width;
	int window_height = hw_surface_get_rect(*window).size.height;
	int img_width = hw_surface_get_rect(images[0]).size.width;
	int img_height = hw_surface_get_rect(images[0]).size.height;

	if (pressed_keys[0] == 1) {		// UP
		if (pos->y > 0) {
			if (pos->y < speed)
				pos->y -= speed - (speed - pos->y);
			else
				pos->y -= speed;
		}
	}

	if (pressed_keys[1] == 1) {		// DOWN
		if (pos->y < window_height - img_height) {
			if (pos->y > window_height - img_height - speed)
				pos->y += window_height - pos->y - img_height;
			else
				pos->y += speed;
		}
	}

	if (pressed_keys[2] == 1) {		// RIGHT
		if (pos->x < window_width - img_width) {
			if (pos->x > window_width - img_width - speed)
				pos->x += window_width - pos->x - img_width;
			else
				pos->x += speed;
		}
	}

	if (pressed_keys[3] == 1) {		// LEFT
		if (pos->x > 0) {
			if (pos->x < speed)
				pos->x -= speed - (speed - pos->x);
			else
				pos->x -= speed;
		}
	}

	hw_surface_lock(images[0]);
	ei_copy_surface(*window, images[0], positions[0], EI_TRUE);
	hw_surface_unlock(images[0]);

	hw_surface_lock(images[1]);
	ei_copy_surface(*window, images[1], positions[1], EI_TRUE);
	hw_surface_unlock(images[1]);

	hw_surface_unlock(*window);
	hw_surface_update_rects(NULL);
}

/*********************************************************************************************/

void moveMouse(ei_surface_t *window, ei_surface_t *images, ei_point_t **positions, ei_point_t cursor_point) {
	int img_width = hw_surface_get_rect(images[1]).size.width;
	int img_height = hw_surface_get_rect(images[1]).size.height;

	ei_point_t *pos = positions[1];
	pos->x = cursor_point.x - img_width / 2;
	pos->y = cursor_point.y - img_height / 2;

	hw_surface_lock(*window);
	ei_fill(*window, &white, EI_FALSE);

	hw_surface_lock(images[1]);
	ei_copy_surface(*window, images[1], positions[1], EI_TRUE);
	hw_surface_unlock(images[1]);

	hw_surface_lock(images[0]);
	ei_copy_surface(*window, images[0], positions[0], EI_TRUE);
	hw_surface_unlock(images[0]);

	hw_surface_unlock(*window);
	hw_surface_update_rects(NULL);
}

/*********************************************************************************************/

int ei_main(int argc, char** argv)
{
	ei_surface_t main_window = NULL;
	ei_size_t main_window_size = { 640, 480 };
	ei_event_t event;

    // Init acces to hardware.
	hw_init();

    // Create the main window.
	main_window = hw_create_window(&main_window_size, EI_FALSE);

	// Fill the main window
    hw_surface_lock(main_window);
    ei_fill(main_window, &white, EI_FALSE);

	// Display first image
	ei_point_t pos_key = { 130 , 130 };
	ei_surface_t image_key = hw_image_load("../../data/ball.png");
	hw_surface_lock(image_key);
	ei_copy_surface(main_window, image_key, &pos_key, EI_TRUE);
	hw_surface_unlock(image_key);

	// Display second image
	ei_point_t pos_mouse = { 0 , 0 };
	ei_surface_t image_mouse = hw_image_load("../../data/ball.png");
	hw_surface_lock(image_mouse);
	ei_copy_surface(main_window, image_mouse, &pos_mouse, EI_TRUE);
	hw_surface_unlock(image_mouse);
	hw_surface_unlock(main_window);
	hw_surface_update_rects(NULL);

    // Set up some variables
	bool quit = false;
	int speed = 10;

	ei_surface_t *images = malloc(sizeof(ei_surface_t)*2);  // contains the 2 images that are displayed
	images[0] = image_key;
	images[1] = image_mouse;
	ei_point_t **img_positions = malloc(sizeof(ei_point_t)*2);  // contains the 2 positions of those images
	img_positions[0] = &pos_key;
	img_positions[1] = &pos_mouse;

	bool *pressed_keys = calloc(4, sizeof(bool));
	int pressed_key;

	double fps_limit = 10.0;
	double time_start = hw_now();
    double time_stop;

    event.type = ei_ev_none;

	while (!quit) {
		hw_event_wait_next(&event);

		switch (event.type) {
			case ei_ev_display :
				if (event.param.display.closed)    // si clic sur la croix, ferme la fenetre
					quit = true;
				break;

			case ei_ev_keydown :
				pressed_key = event.param.key.key_sym;
				if (pressed_key == ALLEGRO_KEY_ESCAPE)
					quit = true;
				if (pressed_key == ALLEGRO_KEY_UP)
					pressed_keys[0] = 1;
				if (pressed_key == ALLEGRO_KEY_DOWN)
					pressed_keys[1] = 1;
				if (pressed_key == ALLEGRO_KEY_RIGHT)
					pressed_keys[2] = 1;
				if (pressed_key == ALLEGRO_KEY_LEFT)
					pressed_keys[3] = 1;
				break;

			case ei_ev_keychar:
				moveKeyboard(&main_window, images, img_positions, speed, pressed_keys);
				break;

			case ei_ev_keyup:
				for (int i=0; i<4; i++)
					pressed_keys[i] = 0;
				break;

			case ei_ev_mouse_move :
                time_stop = hw_now();
				if (time_stop > (time_start + ((double) 1.0 / fps_limit))) {
					time_start = hw_now();

					moveMouse(&main_window, images, img_positions, event.param.mouse.where);
				}
		}
	}

	// Free pointers
	free(images);
	free(img_positions);
	free(pressed_keys);
    // Free hardware resources.
	hw_surface_free(image_key);
	hw_surface_free(image_mouse);
    hw_quit();

    // Terminate program with no error code.
    return 0;
}
