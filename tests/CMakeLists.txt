# tp1 target
add_executable(tp1 tp1.c)
target_link_libraries(tp1 eibase ${Allegro_LIBRARIES} m)

# tp2 target
add_executable(tp2 tp2.c)
target_link_libraries(tp2 eibase ei ${Allegro_LIBRARIES} m)

# tp3 target
add_executable(tp3 tp3.c)
target_link_libraries(tp3 eibase ei ${Allegro_LIBRARIES} m)

# tp4 target
add_executable(tp4 tp4.c)
target_link_libraries(tp4 eibase ei ${Allegro_LIBRARIES} m)

add_executable(tp4_button tp4_button.c)
target_link_libraries(tp4_button eibase ei ${Allegro_LIBRARIES} m)

# tp5 target
add_executable(tp5 tp5.c)
target_link_libraries(tp5 eibase ei ${Allegro_LIBRARIES} m)

# Automatic tests
add_executable(unit_tests unit_tests.cpp)
target_link_libraries(unit_tests eibase ei ${Allegro_LIBRARIES} m)

enable_testing()
ms_add_catch_tests(unit_tests) 
