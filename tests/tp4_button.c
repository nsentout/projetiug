#include <stdlib.h>

#include "ei_types.h"
#include "ei_main.h"
#include "ei_draw.h"
#include "ei_event.h"
#include "hw_interface.h"

#include <math.h>

int ei_main(int argc, char* argv[])
{
    ei_surface_t main_window = NULL;
    ei_size_t    main_window_size;
    ei_event_t   event;
    ei_color_t red   = { 0xff, 0x00, 0x00, 0xff };
    
    ei_linked_point_t* arc_list = arc( (ei_point_t){50,50}, 40, 75, -75);
    
    ei_rect_t rect;
    rect.top_left = (ei_point_t){100,100};
    rect.size = (ei_size_t){200, 100};
    
    // Init acces to hardware.
    hw_init();
    
    // Create the main window.
    main_window_size.width	= 640;
    main_window_size.height	= 480;
    main_window = hw_create_window(&main_window_size, EI_FALSE);
    
    hw_surface_lock(main_window);
    
    ei_fill(main_window, &ei_default_background_color, EI_TRUE);
    
    ei_draw_polyline(main_window, arc_list, red);
    
    draw_button(main_window, rect, 20, "Button");
    
    hw_surface_unlock(main_window);
    hw_surface_update_rects(NULL);
    
    // Wait for a key press.
    event.type = ei_ev_none;
    while (event.type != ei_ev_keydown) {
        hw_event_wait_next(&event);
    }
    
    // Free hardware resources.
    hw_quit();
    
    // Terminate program with no error code.
    return 0;
}
