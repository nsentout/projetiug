#include "ei_draw.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>



static inline ei_color_t alpha_blend(const ei_color_t in_pixel, const ei_color_t dst_pixel)
{
    ei_color_t blended;
    float alpha = ((float)in_pixel.alpha) / 255.f;
    blended.red   = (1.f -  alpha) * dst_pixel.red   + alpha * in_pixel.red;
    blended.green = (1.f -  alpha) * dst_pixel.green + alpha * in_pixel.green;
    blended.blue  = (1.f -  alpha) * dst_pixel.blue  + alpha * in_pixel.blue;
    blended.alpha = 255;
    return blended;
}
/**
 * \brief   Allocate and set the list of points defining an arc
 *
 * @param   center     Center of the arc
 * @param   radius    Radius of the arc
 * @param   start_angle   Beginning angle
 * @param   end_angle   Ending angle
 * @return  Returns the list of points defining the arc.
 */
ei_linked_point_t* arc(ei_point_t center, float radius, int start_angle, int end_angle)
{
    ei_linked_point_t* list = (ei_linked_point_t*) malloc(sizeof(ei_linked_point_t));
    ei_linked_point_t* start = list;
    
    float angle = (end_angle - start_angle) * M_PI/180.f;
    int nbpts = radius * fabs(angle);
    for(int i=0; i<=nbpts; i++){
        list->next = (ei_linked_point_t*) malloc(sizeof(ei_linked_point_t));
        list = list->next;
        list->point.x = radius * cosf(start_angle * M_PI/180.f + (float)i * angle / (float)nbpts) + center.x;
        list->point.y = radius * sinf(start_angle * M_PI/180.f + (float)i * angle / (float)nbpts) + center.y;
        //printf("%d,%d\n", list->point.x, list->point.y);
    }
    list->next = NULL;
    return start->next;
}





typedef enum {BT_TOP, BT_BOTTOM, BT_FULL} bt_part;
/**
 * \brief   Defines the list of points to draw a rounded frame
 *
 * @param   rect    rectangle
 * @param   radius    Radius of the arcs
 * @param   part   Only set the pints for the TOP, BOTTOM or ALL part of the rounded frame
 * @return  Returns the list of points defining the rounded frame
 */
static ei_linked_point_t* rounded_frame(ei_rect_t *rect, float radius, bt_part part)
{
    ei_point_t pt = rect->top_left;
    ei_linked_point_t* list, *start;
    if(part != BT_BOTTOM){
        pt.x += radius;
        pt.y += (rect->size.height - radius);
        list = arc(pt, radius, 135, 180);
        start = list;
        while(list->next)
            list = list->next;
        pt.y -= (rect->size.height - 2.f * radius);
        list->next = arc(pt, radius, 180, 270);
        while(list->next)
            list = list->next;
        pt.x += (rect->size.width - 2.f * radius);
        list->next = arc(pt, radius, 270, 315);
        while(list->next)
            list = list->next;
    }
    
    if(part == BT_TOP) {
        list->next = (ei_linked_point_t*) malloc(sizeof(ei_linked_point_t));
        list = list->next;
        list->point.x = rect->top_left.x + 2*rect->size.width / 3;
        list->point.y = rect->top_left.y + rect->size.height / 2;
        list->next = (ei_linked_point_t*) malloc(sizeof(ei_linked_point_t));
        list = list->next;
        list->point.x = rect->top_left.x + rect->size.width / 3;
        list->point.y = rect->top_left.y + rect->size.height / 2;
        list->next = NULL;
        return start;
    }
    if(part == BT_BOTTOM){
        pt.x += rect->size.width - radius;
        pt.y += radius;
        list = arc(pt, radius, 315, 360);
        start = list;
    }else{
        list->next = arc(pt, radius, 315, 360);
    }
    
    list->next = arc(pt, radius, 315, 360);
    while(list->next)
        list = list->next;
    pt.y += (rect->size.height - 2.f * radius);
    list->next = arc(pt, radius, 0, 90);
    while(list->next)
        list = list->next;
    pt.x -= (rect->size.width - 2.f * radius);
    list->next = arc(pt, radius, 90, 135);
    while(list->next)
        list = list->next;
    if(part == BT_BOTTOM){
        list->next = (ei_linked_point_t*) malloc(sizeof(ei_linked_point_t));
        list = list->next;
        list->point.x = rect->top_left.x + rect->size.width / 3;
        list->point.y = rect->top_left.y + rect->size.height / 2;
        list->next = (ei_linked_point_t*) malloc(sizeof(ei_linked_point_t));
        list = list->next;
        list->point.x = rect->top_left.x + 2*rect->size.width / 3;
        list->point.y = rect->top_left.y + rect->size.height / 2;
        list->next = NULL;
    }
    return start;
}
/**
 * \brief   Draw a rounded  button
 *
 * @param surface     Where to draw the polygon. The surface must be locked* by \ref hw_surface_lock.
 * @param   rect    rectangle
 * @param   radius    Radius of the arcs
 * @param   text    Text to be written on the button
 */
void draw_button(ei_surface_t surface, ei_rect_t rect, float radius, const char* text)
{
    ei_color_t lgrey = { 0xcf, 0xcf, 0xcf, 0xff };
    ei_color_t dgrey = { 0x6f, 0x6f, 0x6f, 0xff };
    ei_color_t white = { 0xff, 0xff, 0xff, 0xff };
    
    ei_linked_point_t* rect_top = rounded_frame(&rect, radius, BT_TOP);
    ei_linked_point_t* rect_bottom = rounded_frame(&rect, radius, BT_BOTTOM);
    rect.top_left.x += 5;
    rect.top_left.y += 5;
    rect.size.width  -= 10;
    rect.size.height -= 10;
    ei_linked_point_t* rect_full = rounded_frame(&rect, radius-5, BT_FULL);
    
    ei_draw_polygon(surface, rect_top, lgrey);
    ei_draw_polygon(surface, rect_bottom, dgrey);
    ei_draw_polygon(surface, rect_full, ei_default_background_color);
    
    // text
    ei_font_t font = hw_text_font_create(ei_default_font_filename, ei_font_default_size);
    int width, height;
    hw_text_compute_size(text,font,&width,&height);
    ei_point_t pos = rect.top_left;
    pos.x += (rect.size.width - width) / 2.f;
    pos.y += (rect.size.height - height) / 2.f;
    ei_draw_text(surface, &pos, text, font, &white);
    
    
}


/**
 * \brief   Compute min and max scanline (in the y direction) for the given polygon
 *
 * @param   first_point     The head of a linked list of the points of the polygon.
 * @param   min_scanline    Stores the minimum scanline
 * @param   max_scanline    Stores the maximum scanline
 */
void min_max_scanline(const ei_linked_point_t* first_point, int* min_scanline, int* max_scanline)
{
    *min_scanline = first_point->point.y;
    *max_scanline = *min_scanline;
    const ei_linked_point_t* lpoint = first_point->next;
    
    while (lpoint != NULL) {
        if (lpoint->point.y < *min_scanline)
            *min_scanline = lpoint->point.y;
        if (lpoint->point.y > *max_scanline)
            *max_scanline = lpoint->point.y;
        lpoint = lpoint->next;
    }
}

/**
 * \brief   Allocate and store all edges of the polygon into an edge_table (ei_edge_t)
 *          in the increasing order of y and x.
 *          For example, edge_table[current_scanline] contains all edges for which
 *          lowest y position is current_scanline + min_scanline
 *
 * @param   first_point     The head of a linked list of the points of the polygon.
 * @param   min_scanline    The minimum scanline
 * @param   max_scanline    The maximum scanline
 */
ei_edge_t** build_edge_table(const ei_linked_point_t* first_point, int min_scanline, int max_scanline)
{
    // Allocate and initialize edge table
    ei_edge_t** edge_table = malloc((max_scanline - min_scanline + 1) * sizeof(ei_edge_t*));
    for (int i = 0; i <= (max_scanline - min_scanline); i++)
        edge_table[i] = NULL;
    
    // Fill edge table
    ei_point_t start = first_point->point;
    const ei_linked_point_t* lpoint = first_point->next;
    ei_bool_t all_processed = EI_FALSE;
    int current_scanline;
    
    // Process polygon edges one by one
    while (!all_processed) {
        if (lpoint == NULL) {
            lpoint = first_point;
            all_processed = EI_TRUE;
        }
        
        ei_point_t end = lpoint->point;
        // skip horizontal edges
        if (start.y != end.y) {
            // Allocate current_edge and fill its fields: y_max, x_min, inv_m
            ei_edge_t* current_edge = malloc(sizeof(struct ei_edge_t));
            if (start.y < end.y) {
                current_edge->y_max = end.y;
                current_edge->x_min = start.x;
                current_edge->inv_m = (float)(end.x - start.x) / (float)(end.y - start.y);
                current_scanline = start.y - min_scanline;
            } else {
                current_edge->y_max = start.y;
                current_edge->x_min = end.x;
                current_edge->inv_m = (float)(start.x - end.x) / (float)(start.y - end.y);
                current_scanline = end.y - min_scanline;
            }
            
            // Insert current_edge in edge table sorted by increasing x of the lower end
            if (edge_table[current_scanline] == NULL) {
                // no prev_edge => insert curent_edge in edge_table[current_scanline]
                edge_table[current_scanline] = current_edge;
                current_edge->next = NULL;
            } else {
                // current_edge y_max superior => iterate over the list and find correct insertion position
                ei_edge_t* tmp_edge = edge_table[current_scanline];
                while (tmp_edge->next != NULL && (tmp_edge->next->y_max <= current_edge->y_max)) {
                    if ((tmp_edge->next->y_max == current_edge->y_max) && (tmp_edge->next->x_min > current_edge->x_min))
                        break;
                    tmp_edge = tmp_edge->next;
                }
                current_edge->next = tmp_edge->next;
                tmp_edge->next = current_edge;
            }
        }
        // Process next edge
        start = end;
        lpoint = lpoint->next;
    }
    
    return edge_table;
}


/**
 * @brief Debugging functions to print the edge tables
 */
void print_edge_table_entry(ei_edge_t* edge)
{
    while (edge != NULL) {
        printf("[%d, %f, %f] ", edge->y_max, edge->x_min, edge->inv_m);
        edge = edge->next;
    }
    printf("\n");
}

void print_edge_table(ei_edge_t** edge_table, int min_scanline, int max_scanline)
{
    ei_edge_t* current_edge;
    printf("Edge table:\n");
    for (int i = 0; i <= (max_scanline - min_scanline); i++) {
        if (edge_table[i] != NULL) {
            printf("%d: ", i + min_scanline);
            current_edge = edge_table[i];
            print_edge_table_entry(current_edge);
        }
    }
    printf("\n");
}

/**
 * \brief Draws a filled polygon.
 *
 * @param surface     Where to draw the polygon. The surface must be locked* by \ref hw_surface_lock.
 * @param first_point The head of a linked list of the points of the polygon.
 *                    It is either NULL (i.e. draws nothing), or has more than 2 points.
 * @param color       The color used to draw the polygon, alpha channel is managed.
 * @return  Returns 0 on success, 1 on failure (no point for the polygon).
 */
int ei_draw_polygon(ei_surface_t surface, const ei_linked_point_t* first_point,
                    const ei_color_t color)
{
    if (first_point == NULL) {
        fprintf(stderr, "no point for the polygon\n");
        return 1;
    }
    
    /* Step 1
     *   Compute min and max scanline (in the y direction) for the given polygon
     */
    int min_scanline, max_scanline;
    min_max_scanline(first_point, &min_scanline, &max_scanline);
    
    
    /* Step 2
     *  Store all edges into an edge_table (ei_edge_t) in the increasing order of y and x.
     */
    ei_edge_t** edge_table = build_edge_table(first_point, min_scanline, max_scanline);
    
    // For debugging purpose
    // print_edge_table(edge_table, min_scanline, max_scanline);
    
    
    ei_edge_t* active_edge_table = NULL;
    ei_edge_t* current_edge, *tmp_edge, *prev_edge;
    
    //loop over all scanlines
    for (int current_scanline = 0; current_scanline <= (max_scanline - min_scanline); current_scanline++) {
        
        /* Step 3
         *   Loop over all edges in the edge table at the current scanline
         *   Insert in active_edge_table all edges that cross this scanline,
         *   in an increasing order of x
         */
        current_edge = edge_table[current_scanline];
        prev_edge = active_edge_table;
        
        while (current_edge != NULL) {
            if (active_edge_table == NULL) {
                active_edge_table = current_edge;
                break;
            } else {
                if (active_edge_table->x_min > current_edge->x_min) {
                    tmp_edge = current_edge->next;
                    current_edge->next = active_edge_table;
                    active_edge_table = current_edge;
                    current_edge = tmp_edge;
                } else {
                    while (prev_edge->next != NULL &&
                           prev_edge->next->x_min < current_edge->x_min) {
                        prev_edge = prev_edge->next;
                    }
                    tmp_edge = current_edge->next;
                    current_edge->next = prev_edge->next;
                    prev_edge->next = current_edge;
                    current_edge = tmp_edge;
                }
            }
        }
        
        /* Step 4
         *  Remove from Active edge table, edges for wich y_max = current_scanline + min_scanline
         */
        current_edge = active_edge_table;
        tmp_edge = NULL;
        while (current_edge != NULL) {
            if (current_edge->y_max == current_scanline + min_scanline) {
                if (tmp_edge == NULL)
                    active_edge_table = current_edge->next;
                else
                    tmp_edge->next = current_edge->next;
            } else {
                tmp_edge = current_edge;
            }
            current_edge = current_edge->next;
        }
        
        
        /* Step 5
         *  Fill the pixels that are inside the polygon (between two edges on the scanline) with alpha
         */
        current_edge = active_edge_table;
        ei_point_t pos;
        ei_color_t p;
        while (current_edge != NULL) {
            pos.y = current_scanline + min_scanline;
            for (pos.x = ceil(current_edge->x_min); pos.x <= floor(current_edge->next->x_min); pos.x++) {
                p = hw_get_pixel(surface, pos);
                hw_put_pixel(surface, pos, alpha_blend(color, p));
            }
            current_edge = current_edge->next->next;
        }
        
        
        /* Step 6
         *  Update the x_min coordinates of the edges in active table
         */
        current_edge = active_edge_table;
        tmp_edge = NULL;
        prev_edge = NULL;
        int i = 0;
        while (current_edge != NULL) {
            // update x_min (similar to Bresenham algorithm)
            current_edge->x_min = current_edge->x_min + current_edge->inv_m;
            
            // Make sure active edge table remains sorted by increasing x
            if ((tmp_edge != NULL) && (tmp_edge->x_min > current_edge->x_min)) {
                // swap entries
                if (prev_edge != NULL) {
                    prev_edge->next = current_edge;
                    tmp_edge->next = current_edge->next;
                    current_edge->next = tmp_edge;
                } else {
                    active_edge_table = current_edge;
                    tmp_edge->next = current_edge->next;
                    current_edge->next = tmp_edge;
                }
                current_edge = tmp_edge;
            }
            prev_edge = tmp_edge;
            tmp_edge = current_edge;
            current_edge = current_edge->next;
            i++;
        }
    }
    
    //free memory
    
    for (int current_scanline = 0; current_scanline <= (max_scanline - min_scanline); current_scanline++) {
        ei_edge_t* current_edge = edge_table[current_scanline];
        //loop over all edges crossing the current scanline
        while (current_edge != NULL) {
            tmp_edge = current_edge;
            current_edge = current_edge->next;
            if (tmp_edge == NULL)
                free(tmp_edge);
        }
    }
    free(edge_table);
    
    return 0;
}


/*
 * Bresenham algorithm, based on octant zero
 * => not symetric when the start and end positions are inverted
 *
 static inline int chooseOctant(const ei_point_t start, const ei_point_t end) {
 float m = (float)(end.y - start.y) / (float)(end.x - start.x);
 if (start.x < end.x) {
 if (m >= 0 && m <= 1)
 return 0;
 if (m < 0 && m >= -1)
 return 7;
 } else {
 if (m > 0 && m <= 1)
 return 4;
 if (m <= 0 && m >= -1)
 return 3;
 }
 
 if (start.y < end.y) {
 if (m > 1)
 return 1;
 if (m < -1)
 return 2;
 } else {
 if (m > 1)
 return 5;
 if (m < -1)
 return 6;
 }
 return 0;
 }
 
 static inline ei_point_t switchToOctantZeroFrom(int octant, ei_point_t pos) {
 ei_point_t new_pos;
 switch (octant) {
 case 0: new_pos = pos; break;
 case 1: new_pos = (ei_point_t) { pos.y,  pos.x}; break;
 case 2: new_pos = (ei_point_t) { pos.y, -pos.x}; break;
 case 3: new_pos = (ei_point_t) { -pos.x,  pos.y}; break;
 case 4: new_pos = (ei_point_t) { -pos.x, -pos.y}; break;
 case 5: new_pos = (ei_point_t) { -pos.y, -pos.x}; break;
 case 6: new_pos = (ei_point_t) { -pos.y,  pos.x}; break;
 case 7: new_pos = (ei_point_t) { pos.x, -pos.y}; break;
 }
 return new_pos;
 }
 
 static inline ei_point_t switchFromOctantZeroTo(int octant, ei_point_t pos) {
 ei_point_t new_pos;
 switch (octant) {
 case 0: new_pos = pos; break;
 case 1: new_pos = (ei_point_t) { pos.y,  pos.x}; break;
 case 2: new_pos = (ei_point_t) { -pos.y,  pos.x}; break;
 case 3: new_pos = (ei_point_t) { -pos.x,  pos.y}; break;
 case 4: new_pos = (ei_point_t) { -pos.x, -pos.y}; break;
 case 5: new_pos = (ei_point_t) { -pos.y, -pos.x}; break;
 case 6: new_pos = (ei_point_t) { pos.y, -pos.x}; break;
 case 7: new_pos = (ei_point_t) { pos.x, -pos.y}; break;
 }
 return new_pos;
 }
 
 void ei_draw_line(ei_surface_t surface, const ei_point_t start,
 const ei_point_t end, const ei_color_t color) {
 ei_point_t pos, start0, end0;
 int octant, diff_x, diff_y, e;
 
 octant = chooseOctant(start, end);
 
 start0 = switchToOctantZeroFrom(octant, start);
 end0 = switchToOctantZeroFrom(octant, end);
 pos = start0;
 diff_x = (end0.x - start0.x);
 diff_y = (end0.y - start0.y);
 e = -diff_x;
 diff_x >>= 1;
 diff_y >>= 1;
 while (pos.x <= end0.x) {
 hw_put_pixel(surface, switchFromOctantZeroTo(octant, pos), color);
 pos.x += 1;
 e += diff_y;
 if (e >= 0) {
 pos.y += 1;
 e -= diff_x;
 }
 }
 }
 */

/**
 * Bresenham algorithm factorized
 */
void ei_draw_line(ei_surface_t surface, const ei_point_t start,
                  const ei_point_t end, const ei_color_t color)
{
    ei_point_t pos = start;
    int dx, dy;
    int stepx, stepy;
    dx = end.x - pos.x;
    dy = end.y - pos.y;
    if (dy < 0) { dy = -dy; stepy = -1; } else { stepy = 1; }
    if (dx < 0) { dx = -dx; stepx = -1; } else { stepx = 1; }
    dy <<= 1; // dy is now 2*dy
    dx <<= 1; // dx is now 2*dx
    
    hw_put_pixel(surface, pos, color);
    if (dx > dy) {
        int fraction = dy - (dx >> 1);
        while (pos.x != end.x) {
            pos.x += stepx;
            if (fraction >= 0) {
                pos.y += stepy;
                fraction -= dx;
            }
            fraction += dy;
            hw_put_pixel(surface, pos, color);
        }
    } else {
        int fraction = dx - (dy >> 1);
        while (pos.y != end.y) {
            if (fraction >= 0) {
                pos.x += stepx;
                fraction -= dy;
            }
            pos.y += stepy;
            fraction += dx;
            hw_put_pixel(surface, pos, color);
        }
    }
}





int ei_draw_polyline(ei_surface_t surface,
                     const ei_linked_point_t* first_point,
                     const ei_color_t color)
{
    ei_point_t start, end;
    
    if (first_point == NULL) {
        fprintf(stderr, " no points for the polyline\n;");
        return 1;
    }
    
    start = first_point->point;
    while (first_point->next != NULL) {
        end = first_point->next->point;
        ei_draw_line(surface, start, end, color);
        start = end;
        first_point = first_point->next;
    }
    return 0;
}








void ei_draw_text(ei_surface_t surface, const ei_point_t* where,
                  const char* text, const ei_font_t font,
                  const ei_color_t* color)
{
    if (text == NULL || color == NULL){
        fprintf(stderr, "no text or color specified");
        return;
    }
    
    ei_surface_t s_text;
    ei_font_t default_font;
    
    if (font == NULL) {
        default_font = hw_text_font_create(ei_default_font_filename, ei_font_default_size);
        s_text = hw_text_create_surface(text, default_font, color);
        hw_text_font_free(default_font);
    } else {
        s_text = hw_text_create_surface(text, font, color);
    }
    
    
    hw_surface_lock(s_text);
    ei_copy_surface(surface, s_text, where, EI_TRUE);
    hw_surface_unlock(s_text);
    
    hw_surface_free(s_text);
}

void ei_fill(ei_surface_t surface, const ei_color_t* color, const ei_bool_t use_alpha)
{
    ei_point_t pos;
    ei_color_t c = color == NULL ? ei_font_default_color : *color;
    ei_color_t p;
    ei_size_t size = hw_surface_get_size(surface);
    for (pos.x = 0; pos.x < size.width; pos.x++) {
        for (pos.y = 0; pos.y < size.height; pos.y++) {
            if (use_alpha) {
                p = hw_get_pixel(surface, pos);
                hw_put_pixel(surface, pos, alpha_blend(c, p));
            } else {
                hw_put_pixel(surface, pos, c);
            }
        }
    }
}



void ei_copy_surface(ei_surface_t destination, const ei_surface_t source,
                     const ei_point_t* where, const ei_bool_t use_alpha)
{
    ei_point_t pos, new_pos;
    
    ei_size_t source_size = hw_surface_get_size(source);
    
    ei_size_t dst_size = hw_surface_get_size(destination);
    
    ei_color_t in_pixel, out_pixel;
    
    ei_point_t top_left = where == NULL ? (ei_point_t){0,0}:*where;
    
    for (pos.x = 0; pos.x < source_size.width; pos.x++) {
        for (pos.y = 0; pos.y < source_size.height; pos.y++) {
            in_pixel = hw_get_pixel(source, pos);
            
            new_pos.x = top_left.x + pos.x;
            new_pos.y = top_left.y + pos.y;
            
            if (new_pos.x>=0 && new_pos.x < dst_size.width
                && new_pos.y>=0 &&  new_pos.y < dst_size.height) {
                if (use_alpha) {
                    out_pixel = hw_get_pixel(destination, new_pos);
                    hw_put_pixel(destination, new_pos, alpha_blend(in_pixel, out_pixel));
                } else {
                    hw_put_pixel(destination, new_pos, in_pixel);
                }
            }
        }
    }
}
